package cbd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CBD {
    
    final static String DB_CONNECTION = "jdbc:neo4j:http://localhost:7474";
    final static String DB_USER = "neo4j";
    final static String DB_PASSWORD = "batata";
    
    public static void main(String[] args) throws SQLException {
        try (Connection con = (Connection) DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD)) {
            
            // Querying
            //String query;
            //query = "CREATE (Caio:Person {name:'Caio Santana', born:1993})";
            //PreparedStatement stmt = con.prepareStatement(query);
            //stmt.executeQuery();
            
            selectRelacaoTipo(5);
        }
    }
    
    public static void insertRestGraph(int restID, int tipoRestID, int freguesiaID) throws SQLException {
        try (Connection con = (Connection) DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD)) {
            
            // Querying
            String query;
            query = "CREATE (rest" + String.valueOf(restID)
                    + ":Restaurante {tipo:" + String.valueOf(tipoRestID)
                    + ", freguesia:" + String.valueOf(freguesiaID)
                    + ", restID:" + String.valueOf(restID) + "})";
            
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.executeQuery();
        }
    }
    
    public static void selectTipoRest(int tipoId) throws SQLException {
        
        try (Connection con = (Connection) DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD)) {
            
            String query;
            query = "MATCH (n:Restaurante {tipo:"+String.valueOf(tipoId)+"}) return n.restID";
            
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()) {
                System.out.println(rs.getString("n.restID"));
            }
        }
    }
    
    public static void createRelacaoTipo(int restTipo, int restId) throws SQLException {
        
        try (Connection con = (Connection) DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD)) {
            String query;
            query = "match (n {tipo:"+String.valueOf(restTipo)+" }),(r1 {restID:"+String.valueOf(restId)+"}) "
                    + "create (r1)-[:TIPO]->(n) create (n)-[:TIPO]->(r1)";
            
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
        }
    }
    
    
    public static void createRelacaoDist(int restId1, int restId2, int patamar) throws SQLException {
        
        try (Connection con = (Connection) DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD)) {
            String query;
            query = "match (n {restID:"+ String.valueOf(restId1)+"}),(r1 {restID:"+String.valueOf(restId2)+"}) "
                    + "create (r1)-[:PROX{patamar:"+String.valueOf(patamar)+"}]->(n)"
                    + " create(n)-[:PROX{patamar:"+String.valueOf(patamar)+"}]->(r1)";
            
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
        }
    }
    
    
    public static void selectRelacaoTipo(int restTipo) throws SQLException {
        
        try (Connection con = (Connection) DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD)) {
            String query;
            query = "MATCH (res:Restaurante {tipo:"+String.valueOf(restTipo)+"})-[:TIPO]->(steveJobs) RETURN res.restID";
            
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()) {
                System.out.println(rs.getString("res.restID"));
            }
        }
    }
    
    public static void selectRelacaoDist(int restID, int patamar) throws SQLException {
        
        try (Connection con = (Connection) DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD)) {
            String query;
            query = "MATCH (res:Restaurante "
                    + "{restID:"+String.valueOf(restID)+"})-[:PROX{patamar:"+String.valueOf(patamar)+"}]->(CJ)"
                    + " RETURN CJ";
            
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()) {
                System.out.println(rs.getString("res.restID"));
            }
        }
    }
}
